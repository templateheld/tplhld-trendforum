<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

get_header(); ?>

    <section id="subsite-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col p-0">
                    <img src="<?php echo get_the_post_thumbnail_url() ?>" alt="<?php echo get_the_title() ?>" class="w-100">
                </div>
            </div>
        </div>
    </section>
    <section id="subsite-title">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <h1><?php echo get_the_title() ?></h1>
                </div>
            </div>
        </div>
    </section>

    <section id="subsite-text">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 main">
                <?php
                // Start the loop.
                while ( have_posts() ) : the_post();
                    the_content();
                endwhile;
                ?>
                <?php
                $partner = get_field('partner');
                ?>
                <?php if( $partner ): ?>
                    <?php foreach ($partner as $partner_detail): ?>
                    <h2><?php echo $partner_detail['partner_title'] ?></h2>
                        <div class="row">
                        <?php foreach ($partner_detail['partner_logo'] as $logos): ?>
                            <div class="col-6 col-lg-4 col-md-4 my-auto text-center p-5">
                                <img src="<?php echo $logos['logo'] ?>" alt="Partner">
                            </div>
                        <?php endforeach; ?>
                            <hr class="divider col-12">
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>

                <?php
                    $tag = get_field('tag');
                    if( $tag ):
                ?>
                    <?php foreach ($tag as $event): ?>
                        <h2><?php echo $event['headline'] ?></h2>
                        <p><?php echo $event['text'] ?></p>
                        <?php foreach ($event['points'] as $day): ?>
                            <div class="row p-2" style="margin-bottom: 2rem; <?php if( $day['break'] == 1 ) { echo ' background: #FFC600'; } ?>">
                                <div class="col-12 col-lg-4">
                                    <p><strong><?php echo $day['time'] ?></strong><br><?php echo $day['topic'] ?></p>
                                </div>
                                <div class="col-12 col-lg-8">
                                    <?php echo $day['text'] ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                <?php endif; ?>

                <?php
                    $speakers = get_field('speakers');
                ?>
                <?php if( $speakers ): ?>
                    <?php foreach ($speakers as $speaker): ?>
                        <div class="row">
                            <div class="col-12 col-lg-4 col-md-4 text-center">
                                <p>
                                    <img src="<?php echo $speaker['image'] ?>" alt="<?php echo $speaker['name'] ?>"><br>
                                    <strong><?php echo $speaker['name'] ?></strong><br>
                                    <?php echo $speaker['title'] ?>
                                </p>
                            </div>
                            <div class="col-12 col-lg-8 col-md-8">
                                <p><?php echo $speaker['text'] ?></p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
                </div>
                <div class="col-12 col-lg-4 text-center sidebar">
                    <a class="btn btn-outline-primary" href="<?php echo get_site_url() ?>/anmeldung/"><h1>Ticket</h1><p>vorbestellen</p></a>
                    <div class="row mx-auto">
                        <div class="col-4"><a target="_blank" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=http://www.trendforum-retail.de', 'newwindow', 'width=600,height=500'); return false;" href="https://www.facebook.com/sharer/sharer.php?u=http://www.trendforum-retail.de"><img src="<?php echo get_template_directory_uri() ?>/images/facebook.svg" alt="Facebook"></a></div>
                        <div class="col-4"><a target="_blank" onclick="window.open('https://www.xing.com/spi/shares/new?url=http://www.trendforum-retail.de', 'newwindow', 'width=600,height=500'); return false;" href="https://www.xing.com/spi/shares/new?url=http://www.trendforum-retail.de"><img src="<?php echo get_template_directory_uri() ?>/images/xing.svg" alt="Xing"></a></div>
                        <div class="col-4"><a target="_blank" onclick="window.open('https://www.linkedin.com/cws/share?url=http://www.trendforum-retail.de&original_referer=http://www.trendforum-retail.com&token=&isFramed=false&lang=de_DE&xd_origin_host=http://www.trendforum-retail.de', 'newwindow', 'width=600,height=500'); return false;" href="https://www.linkedin.com/cws/share?url=http://www.trendforum-retail.de&original_referer=http://www.trendforum-retail.com&token=&isFramed=false&lang=de_DE&xd_origin_host=http://www.trendforum-retail.de"><img src="<?php echo get_template_directory_uri() ?>/images/linkedin.svg" alt="LinkedIn"></a></div>
                    </div>
                    <!--
                    <div class="col"><img class="parnter-logo w-30" src="<?php echo get_template_directory_uri() ?>/images/samsung.png" alt="#"></div>
                    <div class="col"><img class="parnter-logo w-30" src="<?php echo get_template_directory_uri() ?>/images/maggi.png" alt="#"></div>
                    <div class="col"><img class="parnter-logo w-30" src="<?php echo get_template_directory_uri() ?>/images/online.png" alt="#"></div>
                    -->
                </div>
            </div>
        </div>
    </section>


    </main>
</div>
<?php get_footer(); ?>