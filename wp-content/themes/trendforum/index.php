<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

            <section id="cover">

                <img src="<?php echo get_template_directory_uri() ?>/images/main-png.png" alt="#" class="img-fluid w-100">
                <div class="container">
                    <div class="row">
                        <div class="centered">
                            <!-- oberhalbe absolute -->
                            <div class="row">
                                <div class="col-12 text-center">
                                    <h1>Willkommen</h1>
                                    <h2>Digitale Transformation auf der Fläche als Erfolgsgarant</h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="offset-2 col-4">
                                    <p>Dieses hochkarätig, angesetzte Forum möchte die Teilnehmer zu einem Wissensaustausch und Knowhow-Transfer, rund um digitale Trends im Handel, einladen. Die Highlights des Forums bilden digitale Trendsetter im Handel und aus der Industrie.</p>
                                </div>
                                <div class="col-4">
                                    <p>einladen. Die Highlights des Forums bilden digitale Trendsetter im Handel und aus der Industrie.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>

            <section id="separator">
                <div class="container">
                    <div class="row">
                        <div class="col-6 date">
                            <h3>Wann?</h3>
                            <p>09. / 10. Oktober</p>
                        </div>
                        <div class="col-6 place">
                            <h3>Wo?</h3>
                            <p>Samsung Zentrale / 65760 Eschborn</p>
                        </div>
                    </div>
                </div>
            </section>

            <section id="details">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-6 detail-text my-auto">
                            <h1>Trendforum Retail 2018</h1>
                            <h2>Digitale Lösung für den Einzelhandel</h2>
                            <p>In den kommenden Jahren wird der Umsatzanteil im Onlinehandel sicherlich die 20 Prozent Hürde überschreiten. Die Gründe dafür sind vielfältiger Natur und reichen vom Aufwand für die Fahrt, über Verfügbarkeit der Produkte oder Öffnungszeiten bis hin zur Qualität und Quantität an Fachpersonal.</p>
                            <p>Erfolgreiche Storekonzepte zeigen, was notwendig ist, um tatsächlich positiv in die Zukunft schauen zu können.</p>
                           <button type="button" class="btn btn-outline-primary">Anmelden</button>
                        </div>
                        <div class="col-6 picture">
                            <img src="<?php echo get_template_directory_uri() ?>/images/samsungstand.png" alt="#" style= "width:100%;">
                        </div>
                    </div>
                </div>
            </section>

            <section id="separator">
                <div class="container">
                    <div class="row">
                        <div class="col text-center my-auto">
                            <p>Digitalretail – Best Practise aus Handel und Industrie – Trends im Handel</p>
                        </div>
                    </div>
                </div>
            </section>

            <section id="program">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col text-center">
                            <img src="<?php echo get_template_directory_uri() ?>/images/tfr-program.png" alt="#" style= "width:100%;">
                            <div class="centered">
                                <h1>Unser Programm</h1>
                                <p>Wissens- und Austauschplattform zum Thema Digitalisierung im Handel.
                                    <br>Hochkarätige Vorträge und Best Practise Beispiele.
                                    <br>Schlaraffenland für Pixelfans – Besuch des Samsung LED – Showooms.
                                    <br>Storetour durch angesagte #DigitalRetailStores.
                                    <br>Abendevent im Maggi Kochstudio – Zeit zum Networken und Kochevent zum Mitmachen.
                                </p>
                                <button type="button" class="btn btn-outline-primary">Einblick in unser Programm</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="separator">
                <div class="container">
                    <div class="row">
                        <div class="col text-center">
                            <p>Keynotespeaker und Gewinner des Popai Awards 2018 Volker Wissmann / XXX AG</p>
                        </div>
                    </div>
                </div>
            </section>

            <section id="speakers">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-3 speakers-text text-center my-auto">
                            <h1>Referenten</h1>
                            <p>Für unsere Veranstaltung trendforum retail konnten wir angesehene Experten gewinnen, die diesen Tag mit neuen Konzepten, Ideen und Impulsen für den Handel füllen werden. Freuen Sie sich auf spannende Vorträge, lebendige Diskussionen und wertvolle Kontakte!</p>
                             <button type="button" class="btn btn-outline-primary">zu Referenten</button>
                        </div>
                        <div class="col-3">
                            <img class="profilepicture" src="<?php echo get_template_directory_uri() ?>/images/1.png" alt="#" style= "width:100%;">
                            <div class="overlay">
                                <h1>Max Muster</h1>
                                <p>Lorem ipsum dolor sit amet.</p>
                            </div>
                        </div>
                        <div class="col-3">
                            <img class="profilepicture" src="<?php echo get_template_directory_uri() ?>/images/2.png" alt="#" style= "width:100%;">
                            <div class="overlay">
                                <h1>Jannick Böhl</h1>
                                <p>Lorem ipsum dolor sit amet.</p>
                            </div>
                        </div>
                        <div class="col-3">
                            <img class="profilepicture" src="<?php echo get_template_directory_uri() ?>/images/3.png" alt="#" style= "width:100%;">
                            <div class="overlay">
                                <h1>Brigitte Krone</h1>
                                <p>Lorem ipsum dolor sit amet.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="partner">
                <div class="container">
                    <div class="row">
                        <div class="col text-center">
                            <h1>Partner</h1>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row text-center">
                        <div class="col-4">
                            <img class="parnter-logo" src="<?php echo get_template_directory_uri() ?>/images/samsung.png" alt="#">
                        </div>
                        <div class="col-4">
                            <img class="parnter-logo" src="<?php echo get_template_directory_uri() ?>/images/maggi.png" alt="#">
                        </div>
                        <div class="col-4">
                            <img class="parnter-logo" src="<?php echo get_template_directory_uri() ?>/images/online.png" alt="#">
                        </div>
                    </div>
                </div><div class="container">
                    <div class="row text-center">
                        <div class="col">
                            <button type="button" class="btn btn-outline-primary">zu Referenten</button>
                         </div>
                     </div>
                 </div>
             </section>
         </main><!-- #main -->
	</div><!-- #primary -->


<?php
get_footer();

