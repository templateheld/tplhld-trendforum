<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Templateheld
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <title>Trendforum Retail</title>
    <meta property="og:title" content="Trendforum Retail - DIGITALE LÖSUNGEN IM EINZELHANDEL">
    <meta property="og:site_name" content="Trendforum Retail - DIGITALE LÖSUNGEN IM EINZELHANDEL">
    <meta property="og:type" content="company">
    <meta property="og:description" content="Trendforum Retail - DIGITALE LÖSUNGEN IM EINZELHANDEL">
    <meta property="og:url" content="http://www.trendforum-retail.de/">
    <meta property="og:image" content="http://www.trendforum-retail.de/tfr-2.jpg">
    <meta property="og:type" content="article" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<header>
    <nav class="navbar navbar-expand-lg fixed-top navbar-light align-items-center text-center">
        <a class="col-7 col-lg-2 offset-xl-1 navbar-brand text-center" href="<?php echo get_site_url() ?>"><img src="<?php echo get_template_directory_uri() ?>/images/logo-tfr.svg" class="w-75" alt="Trendforum Retail"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="col-12 col-lg-8 col-xl-7 collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo get_site_url() ?>/anmeldung/">Anmeldung</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo get_site_url() ?>/programm/">Programm</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo get_site_url() ?>/referenten/">Referenten</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo get_site_url() ?>/partner/">Partner</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo get_site_url() ?>/review/">Review</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo get_site_url() ?>/kontakt/">Kontakt</a>
                </li>
            </ul>
        </div>
     </nav>
 </header>