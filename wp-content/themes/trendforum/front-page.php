<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

            <?php
                $welcome = get_field('welcome');
            ?>
            <?php if( $welcome ): ?>
            <section id="cover">
                <img src="<?php echo $welcome['background_image'] ?>" alt="<?php echo $welcome['headline'] ?>" class="img-fluid w-100">
                <div class="container">
                    <div class="row">
                        <div class="centered">
                            <!-- oberhalbe absolute -->
                            <div class="row m-0">
                                <div class="col-12 text-center">
                                    <h1><?php echo $welcome['headline'] ?></h1>
                                    <h2><?php echo $welcome['subline'] ?></h2>
                                </div>
                            </div>
                            <div class="d-none d-lg-block">
                                <div class="row m-0 ">
                                    <div class="offset-2 col-4">
                                        <p><?php echo $welcome['column_a'] ?></p>
                                    </div>
                                    <div class="col-4">
                                        <p><?php echo $welcome['column_b'] ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="d-xs-block d-lg-none">
                                <div class="row m-0 d-xs-block d-lg-none">
                                    <div class="col-12">
                                        <p><?php echo $welcome['column_a'].' '.$welcome['column_b'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php endif; ?>

            <?php
            $separator_1 = get_field('separator_1');
            ?>
            <?php if( $separator_1 ): ?>
                <section id="separator">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-lg-6 date">
                            <h3><?php echo $separator_1['headline_1'] ?></h3>
                            <p><?php echo $separator_1['text_1'] ?></p>
                        </div>
                        <div class="col-12 col-lg-6 place">
                            <h3><?php echo $separator_1['headline_2'] ?></h3>
                            <p><?php echo $separator_1['Text_2'] ?></p>
                        </div>
                    </div>
                </div>
            </section>
            <?php endif; ?>

            <?php
                $details = get_field('details');
            ?>
            <?php if( $details ): ?>
            <section id="details">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 col-lg-6 detail-text my-auto">
                            <h1><?php echo $details['headline'] ?></h1>
                            <h2><?php echo $details['subline'] ?></h2>
                            <p><?php echo $details['text'] ?></p>
                            <a href="<?php echo $details['button_url'] ?>" class="btn btn-outline-primary"><?php echo $details['button_text'] ?></a>
                        </div>
                        <div class="col-12 col-lg-6 picture">
                            <img src="<?php echo $details['image'] ?>" alt="<?php echo $details['headline'] ?>" class="w-100">
                        </div>
                    </div>
                </div>
            </section>
            <?php endif; ?>

            <?php
            $separator_2 = get_field('separator_2');
            ?>
            <?php if( $separator_2 ): ?>
            <section id="separator">
                <div class="container">
                    <div class="row">
                        <div class="col text-center my-auto">
                            <p><?php echo $details['headline'] ?></p>
                        </div>
                    </div>
                </div>
            </section>
            <?php endif; ?>

            <?php
            $program = get_field('program');
            ?>
            <?php if( $program ): ?>
            <section id="program">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col text-center">
                            <img src="<?php echo get_template_directory_uri() ?>/images/tfr-program.png" alt="#" style= "width:100%;">
                            <div class="centered">
                                <h1><?php echo $program['headline'] ?></h1>
                                <p><?php echo $program['text'] ?></p>
                                <a href="<?php echo $program['button_url'] ?>" class="btn btn-outline-primary"><?php echo $program['button_text'] ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php endif; ?>

            <?php
            $separator_3 = get_field('separator_3');
            ?>
            <?php if( $separator_3 ): ?>
            <section id="separator">
                <div class="container">
                    <div class="row">
                        <div class="col text-center">
                            <p><?php echo $separator_3['text'] ?></p>
                        </div>
                    </div>
                </div>
            </section>
            <?php endif; ?>

            <?php
            $speakers = get_field('speakers');
            ?>
            <?php if( $speakers ): ?>
            <section id="speakers">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 col-xl-3 speakers-text text-center my-auto">
                            <h1><?php echo $speakers['headline'] ?></h1>
                            <p><?php echo $speakers['text'] ?></p>
                            <a class="btn btn-outline-primary" href="<?php echo $speakers['button_url'] ?>"><?php echo $speakers['button_text'] ?></a>
                        </div>
                        <?php foreach($speakers['images'] as $speaker): ?>
                        <div class="col-12 col-md-4 col-xl-3 mx-auto p-0">
                            <img class="profilepicture w-100" src="<?php echo $speaker['image'] ?>" alt="<?php echo $speaker['name'] ?>">
                            <div class="overlay text-center">
                                <h1><?php echo $speaker['name'] ?></h1>
                                <p><?php echo $speaker['description'] ?></p>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </section>
            <?php endif; ?>

            <?php
            $partner = get_field('partner');
            ?>
            <?php if( $partner ): ?>
            <section id="partner">
                <div class="container">
                    <div class="row">
                        <div class="col text-center">
                            <h1>Partner</h1>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <?php foreach ($partner['sponsor_images'] as $logos): ?>
                        <div class="col-6 col-lg-4 my-auto text-center p-5">
                            <img class="partner-logo" src="<?php echo $logos['image'] ?>" alt="Partner">
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="container">
                    <div class="row text-center">
                        <div class="col">
                            <a class="btn btn-outline-primary" href="<?php echo get_site_url() ?>/partner/">zu unseren Partnern</a>
                         </div>
                     </div>
                 </div>
             </section>
            <?php endif; ?>
         </main>
	</div>


<?php
get_footer();

