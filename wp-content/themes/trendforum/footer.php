<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Templateheld
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
        <section id="boilerplate">
        <div class="container-fluid">
            <div class="row">
                <div class="col p-0">
                    <img class="profilepicture" src="<?php echo get_template_directory_uri() ?>/images/Unbenannt-2.png" alt="#" style= "width:100%;">
                </div>
            </div>
        </div>
        </section>

        <section id="footer">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <p><a href="#">info@trendforum-retail.de</a> <span style="color: #bebd7f;">|</span style="color: #bebd7f;"> <a href="#">T. +49-(0)-5233-954531</a></p>
                        <p>©2018 Trendforum Retail <span style="color: #bebd7f;">|</span style="color: #bebd7f;"> <a href="<?php echo get_site_url() ?>/impressum">Impressum</a> <span style="color: #bebd7f;">|</span style="color: #bebd7f;"> <a href="<?php echo get_site_url() ?>/datenschutz">Datenschutz</a></p>
                    </div>
                </div>
            </div>
        </section>


	</footer>
</div>

<?php wp_footer(); ?>

<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>



<script>
    window.addEventListener("load", function(){
        window.cookieconsent.initialise({
            "palette": {
                "popup": {
                    "background": "#32363b",
                    "text": "#ffffff"
                },
                "button": {
                    "background": "transparent",
                    "text": "#ffffff",
                    "border": "#ffffff"
                }
            },
            "content": {
                "message": "Cookies helfen uns bei der Bereitstellung unserer Dienste. Durch die Nutzung unserer Dienste erklären Sie sich damit einverstanden, dass wir Cookies setzen.",
                "dismiss": "OK",
                "link": "Weitere Informationen zu Cookies und Datenschutz finden Sie hier.",
                "href": "http://www.trendforum-retail.de/datenschutz/"
            }
        })});
</script>

</body>
</html>
